var https = require('https');
var http = require('http');
var express = require('express');
var fs = require('fs');

var hskey = fs.readFileSync('hacksparrow-key.pem');
var hscert = fs.readFileSync('hacksparrow-cert.pem')


var options = {
    key: hskey,
    cert: hscert
};

var app = express();

app.get('/', function (req, res, next) {
    
    console.log(req.secure);
    function redirectSec(req, res, next) {
        if (req.headers['x-forwarded-proto'] == 'http') { 
            res.redirect('https://' + req.headers.host + req.path);
        } else {
            //return next();
        
            return next();
        }
    }

    
    });
app.get('/test', function (req, res, next) {

      res.send(200,JSON.stringify(req.query));
    });


https.createServer(options, app).listen(8000);
